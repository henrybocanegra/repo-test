# No Deseados

Single Web Application (SPA) para la gestión de personas no deseadas.
#### Características
  - Registro / Actualización de personas naturales y jurídicas.
  - Asignación de marcas de no deseados.
  - Búsqueda de no deseados por datos básicos y por marcas correspondientes. Permite obtener resultados aproximados por nombre y apellidos.
  - Registro masivo de personas no deseadas mediante carga de archivo.
  - Configuración de marcas.

### Instalación
La aplicación se ha desarrollado con Polymer 2.
Pasos para ejecutar en un entorno local:
1. Descarga del repositorio
```sh
git clone https://bitbucket.org/techupernodes/nodeseados.git
```
2. Dependencias
```sh
$ npm install
$ bower install
```
3. Ejecución
```sh
polymer serve
```
Ingresar a la ruta:
```sh
http://127.0.0.1:8081
```

### Despliegue en heroku
1. Crear proyecto con buildpack para polymer 2
```sh
heroku create --buildpack https://github.com/hbocanegrac/heroku-buildpack-polymer.git
```
2. Cambiar nombre (opcional)
```sh
heroku apps:rename nodeseados
```
3. Despliegue
```sh
git push heroku master
```
Ingresar a la aplicación:
```sh
https://nodeseados.herokuapp.com
```